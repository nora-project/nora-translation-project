

cp rom/clean_rom/data/cmn/eng/zuk_index_list.dat patches/batch/input/zuk_index_list.dat
cp rom/mod/data/cmn/eng/item.dat patches/batch/input/item.dat
cp rom/mod/data/cmn/eng/ba_monster.dat patches/batch/input/ba_monster.dat

cp patches/nora_se_translations/cmn/item.nset patches/batch/input/item.nset
cp patches/nora_se_translations/cmn/ba_monster.nset patches/batch/input/ba_monster.nset

cp patches/_edited_text/pointers_h/cmn/item_1.dat patches/batch/input/item_1.dat

for i in {2..9}
do
   echo "$i"
   cp patches/_edited_text/pointers_h/cmn/ba_monster_$i.dat patches/batch/input/ba_monster_$i.dat
done
#copy patches/_edited_text/pointers_h/cmn/ba_monster_2.dat patches/batch/input/ba_monster_2.dat
#copy patches/_edited_text/pointers_h/cmn/ba_monster_3.dat patches/batch/input/ba_monster_3.dat
#copy patches/_edited_text/pointers_h/cmn/ba_monster_4.dat patches/batch/input/ba_monster_4.dat
#copy patches/_edited_text/pointers_h/cmn/ba_monster_5.dat patches/batch/input/ba_monster_5.dat
#copy patches/_edited_text/pointers_h/cmn/ba_monster_6.dat patches/batch/input/ba_monster_6.dat
#copy patches/_edited_text/pointers_h/cmn/ba_monster_7.dat patches/batch/input/ba_monster_7.dat
#copy patches/_edited_text/pointers_h/cmn/ba_monster_8.dat patches/batch/input/ba_monster_8.dat
#copy patches/_edited_text/pointers_h/cmn/ba_monster_9.dat patches/batch/input/ba_monster_9.dat

cd patches/batch

./alph-sorting-patcher
#./alph-sorting-patcher.exe

cd ../../
cp patches/batch/output/zuk_index_list.dat rom/mod/data/cmn/eng/zuk_index_list.dat
cp patches/batch/output/item.dat rom/mod/data/cmn/eng/item.dat
cp patches/batch/output/ba_monster.dat rom/mod/data/cmn/eng/ba_monster.dat

read -rsn1 -p"Press any key to continue";echo
