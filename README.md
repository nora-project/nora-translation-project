# Nora Translation Project

_Nora to Toki no Koubou: Kiri no Mori no Majo_ is an JRPG game developed by Index Corporation (c) (Atlus)  for the Nintendo DS. It was released only in Japan in July 21, 2011.

---

This project is mainly focused on two types of modifications as shown in the following screenshots.



## The project text scripts had now been moved to a weblate repository named OpenScripts. Direct modifications to the script files are no longer supported. Any new modifications to the script must be made using the OpenScripts front-end interface at (access permission required):

## [https://open.smolgaming.com/projects/nora-and-the-time-studio/](https://open.smolgaming.com/projects/nora-and-the-time-studio/)

# Screenshots


**Graphics translations**

Request details      |   Status screen   
:------------------:|:--------------------------------------------------:
![](patches/_edited_imgs/edit/imgs/png/scn_request/req_o_windetails000.NCER_0.png)  | ![](patches/_edited_imgs/edit/imgs/png/sta_b_bg0.png) 

Button      |   Buttons Config Background   
:------------------:|:--------------------------------------------------:
![](patches/_edited_imgs/edit/imgs/png/btn_open_menu.png)  | ![](patches/_edited_imgs/edit/imgs/png/config/con_b_bg000.NSCR.png) 

---

**Text scripts translations**

Adventure dialog texts      |   Event dialog texts   
:------------------:|:--------------------------------------------------:
![](screenshots/single/1.png)  | ![](screenshots/single/7.png) 

Battle menu texts      |   Inventory menu texts   
:------------------:|:--------------------------------------------------:
![](screenshots/single/8.png)  | ![](screenshots/single/9.png) 

---

**December 2021 Update: More screenshots**

v0.3c       | v0.3c   
:------------------:|:--------------------------------------------------:
![](screenshots/dual/75.png)  | ![](screenshots/dual/46.png) 
![](screenshots/dual/74.png)  | ![](screenshots/dual/48.png) 
![](screenshots/dual/49.png)  | ![](screenshots/dual/50.png) 
![](screenshots/dual/30.png)  | ![](screenshots/dual/32.png) 
![](screenshots/dual/42.png)  | ![](screenshots/dual/43.png) 
![](screenshots/dual/51.png)  | ![](screenshots/dual/63.png) 
![](screenshots/dual/53.png)  | ![](screenshots/dual/54.png) 
![](screenshots/dual/47.png)  | ![](screenshots/dual/56.png) 

# Usage

* Download the latest release patch from the [releases](https://gitlab.com/nora-project/nora-translation-project/-/releases) page and unzip it.

* Dump your `Nora to Toki no Koubou - Kiri no Mori no Majo` NDS cartridge to obtain a `.nds` rom file. There exist a few number of alternatives publicly available on the internet to do this. Just make sure that whatever method you use, you obtain an untrimmed and clean dump of the game;

  (or) Obtain a copy of this game rom somewhere else.

* Verify the clean `.nds` rom, make sure that it matches the following hashes.

  > **Japanese ROM Information:**
  >
  > ---
  >
  > **CRC-32:** 7625D9DF
  >
  > **MD5:** 49D2731D0F0F751F3CD7A2DA58E5B88A
  >
  > **SHA-256:** 3DC05F7D97C7A931DD49C4257FB3969070D7E1C466187F8E253F357116598A09

* Rename your `.nds` file to `5783 - Nora to Toki no Koubou - Kiri no Mori no Majo (J)(BAHAMUT).nds`

* Copy or move your clean `.nds` rom to the `patch folder`.

* Run the patching script and wait for it to finish.

  > **(Windows)** `3.Apply Patch-Windows.bat`
  >
  > **(Linux)** `3.Apply Patch-Linux.sh`
  >
  > **(Mac)** `3.Apply Patch-Mac.command`

* In case you need the clean rom again, it will be moved to a folder named`old` once the patching process is completed.

* A new `Nora and the Time Studio - The Witch of the Misty Forest.nds`  rom will be created in the `patch folder`,  this is the patched rom. Verify that it matches the following hashes, if it doesn't probably you didn't use the correct clean rom.

* (Optional) If necessary, apply the `nora_apfix.xdelta` AP-fix patch.

  

  #### v0.1-alpha

  ---

  > **Alpha 2020: English Patched ROM Information:**
  >
  > ---
  >
  > **CRC-32:** 68763BDF
  >
  > **MD5:** ED0CD2E9223C762A189DF6626B564A73
  >
  > **SHA-256:** 6EF03A2EBE0F1D64D4B5FE134818A506A64B3F96DC81AA186BD17236C877D86D

---

  #### v0.3-c

---

  > **Release 2022: English Patched ROM Information:**
  >
  > ---
  >
  > **CRC-32:** 2F29A9A4
  >
  > **MD5:** 724CDD4D32D58AF9023ACD58923F6A17
  >
  > **SHA-256:** 71B9498682D806FB68DE5D7A223CE71E434D6AA2A58B1331385CC8CCBF4B84A4

---

  #### v0.3-d

---

  > **Release 2022: English Patched ROM Information:**
  >
  > ---
  >
  > **CRC-32:** 8AD6FB6B
  >
  > **MD5:** AF802FC9611A03EACF8D10BB3D5A9B5F
  >
  > **SHA-256:** 778F30A4C8A83DA13F717126DCE25D81C1F3268A9B5D30377D7200B65F815816

---

  #### v0.3-d-1

---

  > **Release 2022: English Patched ROM Information:**
  >
  > ---
  >
  > **CRC-32:** 1892576A
  >
  > **MD5:** 9AD45EA9EC88D004537E98A9D60A6525
  >
  > **SHA-256:** DBF81250F7B3B2482A393C6E787ECA7FDD94FEEC510AAC745919907144F75808

---

There is a wiki documentation under construction, if you are interested in knowing what's the methodology used to create this patch, please visit the following link.

[Wiki](https://gitlab.com/nora-project/nora-translation-project/-/wikis/home)
---

# Changelog

#### v0.1a (2020-03-27)

---

**Initial commit**

---

***Graphics translations***

* The main menu buttons have been translated and edited
* Most of the in-game menu buttons has been translated and edited
* Some of the in-game text boxes have been resized to fit English dialogues
* A couple of the characters name labels used in the dialogues have been translated, edited and resized
* Inventory graphic labels and backgrounds have been translated and edited.
* The Exchange rate bar has been translated and edited.
* Status menus and boxes have been translated and edited.
* Item menus and boxes have been translated and edited.
* Equipment management boxes and labels have been translated and edited.
* Skills information box has been translated and edited.
* Item processing, refining and transforming labels have been translated and edited.
* Item category labels have been edited and translated.
* Item description background has been edited and translated.
* Request menus and boxes have been translated and edited.
* Request status labels have been edited and translated.
* Formation box has been translated and edited.
* Sound configuration background has been translated and edited.
* Button configuration background have been translated and edited.
* Vademecum background and title labels have been translated and edited.
* Adventure window on/off button has been translated and edited.
* Battle boxes have been translated and edited.
* The garden logboard has been translated and edited.
* Load and Save background has been translated and edited.
* The received requests list background has been translated and edited.
* Money background has been translated and edited.

---

***Text scripts translations***

* The first event scripts have been translated as a demo.
* Battle texts menus have been translated.
* Configuration menus have been translated.
* Vademecum menus have been translated.
* Collecting menus and dialogues have been translated.
* Adventure game paused texts have been translated.
* Load and Save texts have been translated.
* Equipment management texts have been translated.
* Adventure formation management texts have been translated.
* Town map Nora phrases have been translated.

#### v0.3c (2022-04-03)

---

**TODO. Meanwhile, check the last year commits.**

---

#### v0.3d (2022-09-28)

---

***Graphics translations***

* The title screen version updated.
* Move item R, L, Select and Start buttons translated.
* Font fixed for a few characters.
* Some tutorial images where modified to match the terminology used in-game.


***Text scripts translations***

* Some broken scripts that appear as untranslated had now been fixed.
* Characters description know shouldn't overflow the screen.
* A couple item names were shortened to fit on screen.
* Typos here and there were corrected.
* Inconsitent naming of items had been addressed.

Big thanks to the users at gbatemp who reported most of the above mistakes in the previous version.

#### v0.3d-1 (2022-10-08)

---

* Hardcoded AP-fix has been removed as it produced incompatibility with flashcarts in-built AP patching. Now a separate AP-fix xdelta patch is provided separately, but it shouldn't be necessary most of the time. No need to update if you're playing on emulator or TwilightMenu++ with nds-bootstrap.
* A bug that froze the game on flashcarts when trying to open the alphabetical sorting section of the game's items guide, has now been fixed. Again, no need to update if you're playing on emulator or TwilightMenu++ with nds-bootstrap..

---

# Credits

* Tinke https://github.com/pleonex/tinke
* Swiss File Knife https://sourceforge.net/projects/swissfileknife/
* Crystal Tile 2 https://www.romhacking.net/utilities/818/
* Aseprite https://www.aseprite.org/
* HxD https://mh-nexus.de/en/hxd/

