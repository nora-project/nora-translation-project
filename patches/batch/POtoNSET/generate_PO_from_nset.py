import os
import sys
import POtoNSET
# assign directory
directory = 'files'

print(os.getcwd())
 
# iterate over files in
# that directory
def main():
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        # checking if it is a file
        if os.path.isfile(f):
            print(f)
            #arg1 file to generate
            #arg2 file path
            #arg3 mode "0" only PO, mode "1" POT and PO
            POtoNSET.main("-toPO", f,"1")

if __name__ == "__main__":
    main()