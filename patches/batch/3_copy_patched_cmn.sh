cat patches/_edited_text/cmn_translations.txt | while read line || [ -n "$line" ];
do
  stringarray=($line)
  echo ${stringarray[0]}
  cp patches/_edited_text/_data_patched/cmn/${stringarray[0]}.dat patches/_patched_tree/texts/data/cmn/eng/${stringarray[0]}.dat
done

#for /F "tokens=*" %%A in (patches/_edited_text/cmn_translations.txt) do (
#for /f "tokens=1 delims= " ${stringarray[0]} in ("%%A") do (
#xcopy /s /y /f patches/_edited_text/_data_patched/cmn/${stringarray[0]}.dat patches/_patched_tree/texts/data/cmn/eng/${stringarray[0]}.dat*
#  )
#)

cat patches/_edited_text/cmn_translations_special_1.txt | while read line || [ -n "$line" ];
do
  stringarray=($line)
  echo ${stringarray[0]}
  cp patches/_edited_text/_data_patched/cmn/${stringarray[0]}.dat patches/_patched_tree/texts/data/cmn/eng/${stringarray[0]}.dat
done

#for /F "tokens=*" %%A in (patches/_edited_text/cmn_translations_special_1.txt) do (
#for /f "tokens=1 delims= " ${stringarray[0]} in ("%%A") do (
#xcopy /s /y /f patches/_edited_text/_data_patched/cmn/${stringarray[0]}.dat patches/_patched_tree/texts/data/cmn/eng/${stringarray[0]}.dat*
#  )
#)

cat patches/_edited_text/cmn_translations_special_2.txt | while read line || [ -n "$line" ];
do
  stringarray=($line)
  echo ${stringarray[0]}
  cp patches/_edited_text/_data_patched/cmn/${stringarray[0]}.dat patches/_patched_tree/texts/data/cmn/eng/${stringarray[0]}.dat
done

#for /F "tokens=*" %%A in (patches/_edited_text/cmn_translations_special_2.txt) do (
#for /f "tokens=1 delims= " ${stringarray[0]} in ("%%A") do (
#xcopy /s /y /f patches/_edited_text/_data_patched/cmn/${stringarray[0]}.dat patches/_patched_tree/texts/data/cmn/eng/${stringarray[0]}.dat*
#  )
#)