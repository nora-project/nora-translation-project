cat patches/_edited_text/scn_translations.txt | while read line || [ -n "$line" ];
do
   stringarray=($line)
   echo ${stringarray[0]}
  ~/Documents/Qt/nora-se-build/NoraScriptEditor --export patches/nora_se_translations/scripts/${stringarray[0]}.nset patches/_edited_text/nora.tbl patches/_edited_text/text_h_trans/scripts/${stringarray[0]}.txt patches/_edited_text/pointers_h_trans/scripts/${stringarray[0]}.txt --absolute ${stringarray[1]} --noMsgs --module default.nsem

done
