cp rom/clean_rom/data/cmn/eng/ba_monster.dat patches/_edited_text/_data_temp/cmn/ba_monster.dat
#xcopy /F /Y rom/clean_rom/data/cmn/eng/ba_monster.dat patches/_edited_text/_data_temp/cmn/ba_monster.dat*

for i in {1..9}
do
   echo "$i"
   ~/Documents/Qt/nora-se-build/NoraScriptEditor --insert patches/_edited_text/_data_temp/cmn/ba_monster.dat patches/_edited_text/pointers_h/cmn/ba_monster_$i.dat patches/_edited_text/pointers_h_trans/cmn/ba_monster.txt patches/_edited_text/text_h/cmn/ba_monster.dat patches/_edited_text/text_h_trans/cmn/ba_monster.txt patches/_edited_text/_data_temp/cmn/ba_monster.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/ba_monster.txt
done
#for /L %%B IN (1,1,9) DO (
#C:/Users/migue/Documents/Dev/Qt/build/NoraScriptEditor.exe --insert patches/_edited_text/_data_temp/cmn/ba_monster.dat patches/_edited_text/pointers_h/cmn/ba_monster_%%B.dat patches/_edited_text/pointers_h_trans/cmn/ba_monster.txt patches/_edited_text/text_h/cmn/ba_monster.dat patches/_edited_text/text_h_trans/cmn/ba_monster.txt patches/_edited_text/_data_temp/cmn/ba_monster.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/ba_monster.txt
#)
cp patches/_edited_text/_data_temp/cmn/ba_monster.dat patches/_edited_text/_data_patched/cmn/ba_monster.dat
#xcopy /F /Y patches/_edited_text/_data_temp/cmn/ba_monster.dat patches/_edited_text/_data_patched/cmn/ba_monster.dat*

cp rom/clean_rom/data/cmn/eng/chara.dat patches/_edited_text/_data_temp/cmn/chara.dat
#xcopy /F /Y rom/clean_rom/data/cmn/eng/chara.dat patches/_edited_text/_data_temp/cmn/chara.dat*

for i in {1..2}
do
   echo "$i"
   ~/Documents/Qt/nora-se-build/NoraScriptEditor --insert patches/_edited_text/_data_temp/cmn/chara.dat patches/_edited_text/pointers_h/cmn/chara_$i.dat patches/_edited_text/pointers_h_trans/cmn/chara.txt patches/_edited_text/text_h/cmn/chara.dat patches/_edited_text/text_h_trans/cmn/chara.txt patches/_edited_text/_data_temp/cmn/chara.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/chara.txt
done
#for /L %%B IN (1,1,2) DO (
#C:/Users/migue/Documents/Dev/Qt/build/NoraScriptEditor.exe --insert patches/_edited_text/_data_temp/cmn/chara.dat patches/_edited_text/pointers_h/cmn/chara_%%B.dat patches/_edited_text/pointers_h_trans/cmn/chara.txt patches/_edited_text/text_h/cmn/chara.dat patches/_edited_text/text_h_trans/cmn/chara.txt patches/_edited_text/_data_temp/cmn/chara.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/chara.txt
#)

cp patches/_edited_text/_data_temp/cmn/chara.dat patches/_edited_text/_data_patched/cmn/chara.dat
#xcopy /F /Y patches/_edited_text/_data_temp/cmn/chara.dat patches/_edited_text/_data_patched/cmn/chara.dat*

cp rom/clean_rom/data/cmn/eng/item.dat patches/_edited_text/_data_temp/cmn/item.dat
#xcopy /F /Y rom/clean_rom/data/cmn/eng/item.dat patches/_edited_text/_data_temp/cmn/item.dat*

for i in {1..1}
do
   echo "$i"
   ~/Documents/Qt/nora-se-build/NoraScriptEditor --insert patches/_edited_text/_data_temp/cmn/item.dat patches/_edited_text/pointers_h/cmn/item_$i.dat patches/_edited_text/pointers_h_trans/cmn/item.txt patches/_edited_text/text_h/cmn/item.dat patches/_edited_text/text_h_trans/cmn/item.txt patches/_edited_text/_data_temp/cmn/item.dat --first --noMsgs patches/_edited_text/pointers_from_text_h/cmn/item.txt
done
#for /L %%B IN (1,1,1) DO (
#C:/Users/migue/Documents/Dev/Qt/build/NoraScriptEditor.exe --insert patches/_edited_text/_data_temp/cmn/item.dat patches/_edited_text/pointers_h/cmn/item_%%B.dat patches/_edited_text/pointers_h_trans/cmn/item.txt patches/_edited_text/text_h/cmn/item.dat patches/_edited_text/text_h_trans/cmn/item.txt patches/_edited_text/_data_temp/cmn/item.dat --first --noMsgs patches/_edited_text/pointers_from_text_h/cmn/item.txt
#)

for i in {2..3}
do
   echo "$i"
   ~/Documents/Qt/nora-se-build/NoraScriptEditor  --insert patches/_edited_text/_data_temp/cmn/item.dat patches/_edited_text/pointers_h/cmn/item_$i.dat patches/_edited_text/pointers_h_trans/cmn/item.txt patches/_edited_text/text_h/cmn/item.dat patches/_edited_text/text_h_trans/cmn/item.txt patches/_edited_text/_data_temp/cmn/item.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/item.txt
done
#for /L %%B IN (2,1,3) DO (
#C:/Users/migue/Documents/Dev/Qt/build/NoraScriptEditor.exe --insert patches/_edited_text/_data_temp/cmn/item.dat patches/_edited_text/pointers_h/cmn/item_%%B.dat patches/_edited_text/pointers_h_trans/cmn/item.txt patches/_edited_text/text_h/cmn/item.dat patches/_edited_text/text_h_trans/cmn/item.txt patches/_edited_text/_data_temp/cmn/item.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/item.txt
#)

for i in {4..6}
do
   echo "$i"
   ~/Documents/Qt/nora-se-build/NoraScriptEditor --insert patches/_edited_text/_data_temp/cmn/item.dat patches/_edited_text/pointers_h/cmn/item_$i.dat patches/_edited_text/pointers_h_trans/cmn/item.txt patches/_edited_text/text_h/cmn/item.dat patches/_edited_text/text_h_trans/cmn/item.txt patches/_edited_text/_data_temp/cmn/item.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/item_4_6.txt
done
#for /L %%B IN (4,1,6) DO (
#C:/Users/migue/Documents/Dev/Qt/build/NoraScriptEditor.exe --insert patches/_edited_text/_data_temp/cmn/item.dat patches/_edited_text/pointers_h/cmn/item_%%B.dat patches/_edited_text/pointers_h_trans/cmn/item.txt patches/_edited_text/text_h/cmn/item.dat patches/_edited_text/text_h_trans/cmn/item.txt patches/_edited_text/_data_temp/cmn/item.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/item_4_6.txt
#)

cp patches/_edited_text/_data_temp/cmn/item.dat patches/_edited_text/_data_patched/cmn/item.dat
#xcopy /F /Y patches/_edited_text/_data_temp/cmn/item.dat patches/_edited_text/_data_patched/cmn/item.dat*

cp rom/clean_rom/data/cmn/eng/garden_event.dat patches/_edited_text/_data_temp/cmn/garden_event.dat
#xcopy /F /Y rom/clean_rom/data/cmn/eng/garden_event.dat patches/_edited_text/_data_temp/cmn/garden_event.dat*

for i in {1..2}
do
   echo "$i"
   ~/Documents/Qt/nora-se-build/NoraScriptEditor --insert patches/_edited_text/_data_temp/cmn/garden_event.dat patches/_edited_text/pointers_h/cmn/garden_event_$i.dat patches/_edited_text/pointers_h_trans/cmn/garden_event.txt patches/_edited_text/text_h/cmn/garden_event.dat patches/_edited_text/text_h_trans/cmn/garden_event.txt patches/_edited_text/_data_temp/cmn/garden_event.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/garden_event.txt
done
#for /L %%B IN (1,1,2) DO (
#C:/Users/migue/Documents/Dev/Qt/build/NoraScriptEditor.exe --insert patches/_edited_text/_data_temp/cmn/garden_event.dat patches/_edited_text/pointers_h/cmn/garden_event_%%B.dat patches/_edited_text/pointers_h_trans/cmn/garden_event.txt patches/_edited_text/text_h/cmn/garden_event.dat patches/_edited_text/text_h_trans/cmn/garden_event.txt patches/_edited_text/_data_temp/cmn/garden_event.dat --multi --noMsgs patches/_edited_text/pointers_from_text_h/cmn/garden_event.txt
#)

cp patches/_edited_text/_data_temp/cmn/garden_event.dat patches/_edited_text/_data_patched/cmn/garden_event.dat
#xcopy /F /Y patches/_edited_text/_data_temp/cmn/garden_event.dat patches/_edited_text/_data_patched/cmn/garden_event.dat*